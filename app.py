from ast import arg
import os
import json
import argparse


def format_environment_vars(env_vars, format) -> list:
    """
    Returns an object in the described format.

        Parameters:
            env_vars (dict): A dictionary
            format (str): A string describing the format that the object should be returned in. 

        Returns:
            An object in the specified format
    """
    env_vars = [env_vars]
    if format != "json":
        raise Exception("${format} not a valid format")
    else:
        return json.dumps(env_vars, indent=4)


def get_os_environment_vars(excludes) -> dict:
    """
    Returns a dictionary representation of environment variables.

        Parameters:
            excludes (list): A list of environment variables to exclude from retrieving.

        Returns:
            Returns a dictionary of os environment variables
    """
    env_vars = {}
    for k, v in os.environ.items():
        if (k.lower() not in excludes):
            env_vars[k] = v
    return env_vars


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Get environment variables")
    parser.add_argument("-f",
                        "--format",
                        choices=["json"],
                        default="json",
                        type=str,
                        help="Output format of environment variables")
    parser.add_argument("-e",
                        "--exclude",
                        default=["ps1", "ls_color"],
                        nargs="*",
                        type=str,
                        help="List of environment variables to ignore.")
    parser.add_argument("-i",
                        "--include-all",
                        action="store_true",
                        help="Include all environment variables, ignoring any added to the excludes flag")
    args = parser.parse_args()

    # If the --include-all flag, then get all os environment variables.
    # Otherwise, use the default list or add additional environment variables to ignore.
    exclude = [] if args.include_all else args.exclude

    env_vars = get_os_environment_vars(exclude)
    formated_environment_vars = format_environment_vars(env_vars, args.format)
    print(formated_environment_vars)
